#!/bin/bash

# Date: 11/20/2021
# Author: Colin Leftwich
# This script configures ansible to automate system configuration

####################################################################
#                                                                  #
#  Distro specific commands are separated into their own function  #
#                                                                  # 
####################################################################

# For distros that use the pacman package manager
bootstrap-pacman () {

    installed=();

    echo -e "Running pacman commands\n"
    # pacman -y -Syu

    for PKG; do

	# Gets status of pacakge from pacman
	PKG_LIST=$(pacman -Qs "$PKG" | grep "local" | awk 'BEGIN {FS = "/"} {print $2}' | awk '{print $1}' | grep -w "$PKG")

	for MATCH in $PKG_LIST; do
	    if [[ "$MATCH" == "$PKG" ]]; then
		PKG_STATUS=$PKG
		break
	    fi
	done

	# Installs package if not already installed
	if [[ "$PKG_STATUS" == "$PKG" ]]; then
	    printf "%s already installed\n" "$PKG"
	else
	    printf "%s not found\n" "$PKG"
	    installed+=("$PKG")
	    sudo pacman -S --noconfirm $PKG
	fi

    done;

    printf "\n"
    if [ -n "$installed" ]; then
	printf "Packages Installed:%s\n" "${toInstall[@]}";
    fi 
	
}

# For distros that use the apt package manager
bootstrap-apt () {

    installed=();

    echo -e "Running apt commands\n"
    # sudo apt-get -y update && sudo apt-get -y full-upgrade

    for PKG; do

	# Gets status of pacakge from apt
	PKG_STATUS=$(dpkg -l | grep $PKG | awk '{print $2}' | head -1)

	# Installs package if not already installed
	if [[ "$PKG_STATUS" == "$PKG" ]]; then
	    printf "%s already installed\n" "$PKG"
	else
	    printf "%s not found\n" "$PKG"
	    installed+=(${PKG})
	    sudo apt-get -y install $PKG
	fi

    done;

    printf "\n"
    if [ -n "$installed" ]; then
	printf "Packages Installed:%s\n" "${toInstall[@]}";
    fi 

}


################################################
#                                              #
#  Installs prerequisite packages for ansible  #
#                                              # 
################################################

main () {

    # Distro and package list
    DISTRO=$(hostnamectl | grep "Operating System" | awk '{print $3}')
    packages=("git" "ansible" "inetutils");

    # Matches distro to the appropriate package manager
    case $DISTRO in
	
	Ubuntu | Pop!_os)
	    bootstrap-apt "${packages[@]}";
	    ;;
	
	Manjaro | Arch)
	    bootstrap-pacman "${packages[@]}";
	    ;;
	
	Rocky)
	    echo "Fill me in later"
	    ;;
	
	*)
	    echo "unknown"
	    ;;
	
    esac
}

main;


# set up variables
IP_ADDR=$(hostname -i | awk '{ print $1 }')
INV=inventory
SERVER=""
WORKSTATION=""


# prompt user for role
echo "Server or Workstation"
echo "Server (1)"
echo "Workstation (2)"
read -p "Enter: " INPUT

if [[ "$INPUT" == 1 ]]; then
    SERVER="$IP_ADDR"
    ROLE="1"
elif [[ "$INPUT" == 2 ]]; then
    WORKSTATION="$IP_ADDR"
    ROLE="1"
else
    echo "non-number"
    exit 1
fi

# Makes sure the role is not empty
if [[ -z "$ROLE" ]]; then
   printf '%s\n' "No input entered"
   exit 1
fi

if test -f "$INV"; then
    echo "$INV exists."
    rm $INV
fi

touch inventory
echo "[localhost]" >> inventory
echo $IP_ADDR >> inventory
echo "" >> inventory

echo "[workstation]" >> inventory
echo "$WORKSTATION" >> inventory
echo "" >> inventory
echo "[server]" >> inventory
echo "$SERVER" >> inventory

sudo ansible-pull -i inventory -U https://gitlab.com/ColinLeftwich/ansible_test_project.git 
